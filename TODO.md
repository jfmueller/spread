Areas for improvement:

1. Excel functionality
----------------------

* Pivot tables: This could transform a region of cells into a tree-structure (representing the group-by results)

* Read/write XSLX: The .xslx forma is just a zipped folder with xml files, so we could read and write it easily (without having to use COM)

* More functions: Support more Excel functions

* Analyse Excel formulae: Analyse the AST to identify errors, etc. 

2. User Interface
-----------------

* Web interface: Add a web interface to make it look like a proper spreadsheet. This should be in a separate package

* Command line: Once xslx files can be processes, I want to write a command line app for analysing spreadsheets 