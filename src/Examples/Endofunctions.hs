{-# LANGUAGE OverloadedStrings #-}
module Examples.Endofunctions where

import Control.Applicative hiding (empty)
import Control.Monad hiding (ap)

import Spread.Core.RefFunc hiding (ap, val)
import qualified Spread.Core.RefFunc as R
import Spread.Core.Region
import Spread.Expression.Endofunction

import Prelude hiding (lookup)

type IntFunc f = RefFunc f String (Endo Integer)

ap :: (Alternative f, Monad f) => RefFunc f s (Endo a) -> RefFunc f s (Endo a) -> RefFunc f s (Endo a)
ap f v = refFunc $ \m -> join $ apply <$> (unRefFunc f m) <*> (unRefFunc v m)

val :: Applicative f => Integer -> IntFunc f
val = R.val . J

func :: Applicative f => (Integer -> Integer) -> IntFunc f
func o = R.val $ F $ \s -> J $ o s

plusOne :: Applicative f => IntFunc f
plusOne = func (+1)

env :: Region Maybe String (Endo Integer)
env = region $ \s -> case s of
    "x"  -> val 1999
    "addOne" -> plusOne
    "t" -> ap "addOne" "x"
    _   -> empty

results :: Region Maybe s (Endo b) -> s -> Maybe b
results r s = (eval r) s >>= unwrapVal

