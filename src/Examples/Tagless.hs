module Examples.Tagless where

import Control.Applicative hiding (empty)

import Spread.Expression.Language
import Spread.Expression.Evaluate
import Spread.Expression.Pretty

import Spread.Core.RefFunc
import Spread.Core.Region

val' :: (PrimsY repr,
         Applicative f) => Integer -> RefFunc f s (repr Integer)
val' = val . int

t :: (
    Symantics repr, 
    NumY repr, 
    PrimsY repr,
    Applicative f) => RefFunc f s (repr Integer)
t = val t1

env :: (
    Symantics repr, 
    NumY repr, 
    PrimsY repr) => Region Maybe String (repr Integer)
env = region $ \s -> case s of
    "x" -> val' 1999
    "y" -> val $ (int 100) `add` (int 100)
    _   -> empty 

valueAt :: (Symantics repr, 
    NumY repr, 
    PrimsY repr) => String -> Maybe (repr Integer)
valueAt = eval env

-- | Print the expression in a cell
--   This is what you would see in the formula editor in
--   Excel.
prettyValueAt :: String -> Maybe (Pretty Integer)
prettyValueAt = valueAt

-- | Evaluate the expression in a cell
evaluateAt :: String -> Maybe (EvaluateAs Integer)
evaluateAt = valueAt