module Examples.Excel where

import Control.Applicative hiding (empty)
import qualified Data.Map as M

import Spread.Core.RefFunc hiding (split')
import Spread.Core.Region
import Spread.Excel.Types


import Prelude

import Spread.Symbol.Grid

-- This is an example of RefFuncs with some more sophisticated features 
-- inspired by MS Excel, for example:
-- references to (ranges of) cells, 
-- functions on lists of cells (MAX etc),
-- VLOOKUPs
-- Pivot table

-- WORK IN PROGRESS!

-- | Some cells to be used in examples
a1 = origin
a2 = down a1
a3 = down a2
a4 = down a3
a5 = down a4
a6 = down a5
a7 = down a6
a8 = down a7

worksheet1 :: Region Maybe Cell ExcelExpr
worksheet1 = fromMap $ M.fromList [
    -- Row 1
    (a1, s "Earnings"),
    (right a1,  s "Profit"),
    (right $ right a1, s "Year"),
    -- Row 2
    (a2, i 10),
    (right a2, i 8),
    (right $ right a2, i 2010),
    -- Row 3
    (a3, i 11),
    (right a3, i 8),
    (right $ right a3, i 2011),
    -- Row 4
    (a4, i 13),
    (right a4, i 10),
    (right $ right a4, i 2012),
    -- Row 5
    (a5, i 11),
    (right a5, i 9),
    (right $ right a5, i 2013),
    -- Row 6
    (a6, i 12),
    (right a6, i 9),
    (right $ right a6, i 2014),
    -- Row 7
    (a7, s "Highest Earnings"),
    (right a7, val $ Max $ range a2 a6),
    -- Row 8
    (a8, s "Best Year"),
    (right a8, val $ VLookup (Max $ range a2 a6) (range a2 c6) 3) -- =VLOOKUP(MAX(A2:A6),A2:C6,3)
     ] where
        c6 = right $ right a6
        s = val . Prim . TString
        i = val . Prim . TInt

-- worksheet2 contains the same cells as worksheet1, but the cells are evaluated
-- to excel primitives (rather than expressions)
worksheet2 :: Region Maybe Cell (ExcelExpr, ExcelPrim)
worksheet2 = split' worksheet1 interpret

-- To check that Max is working correctly (Cell b7 in worksheet1), try:
mx :: Maybe ExcelPrim
mx = fmap snd $ eval worksheet2 $ right a7
-- mx should be (Just (TInt 13)), the highest value in range A2:A6

-- vl demonstrates VLOOKUP
-- It evaluates cell b8 
vl :: Maybe ExcelPrim
vl = fmap snd $ eval worksheet2 $ right a8