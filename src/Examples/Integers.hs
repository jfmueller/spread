{-# LANGUAGE OverloadedStrings #-}
module Examples.Integers where

import Control.Monad hiding (ap)
import Control.Applicative hiding (empty)

import Spread.Core.Region
import Spread.Core.RefFunc

r3 :: Applicative f => RefFunc f s (Integer -> Integer)
r3 = val (\x -> x + 1)

env :: Region Maybe String Integer
env = region $ \s -> case s of
    "x" -> 1999
    "t" -> ap (getFunction "increase") ("x" + "x")
    _   -> empty 
    where
        getFunction = eval envF

envF :: Region Maybe String (Integer -> Integer)
envF = region $ \s -> case s of
    "increase" -> r3
    _ -> empty