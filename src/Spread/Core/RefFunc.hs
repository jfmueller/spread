{-# LANGUAGE FlexibleInstances #-}
module Spread.Core.RefFunc(
    RefFunc,
    val,
    ap,
    empty,
    lookup,
    multiply,
    refFunc,
    unRefFunc,
    split,
    split',
    lift2,
    lift1
    ) where

import Control.Applicative hiding (empty)
import qualified Control.Applicative as A
import Control.Monad hiding (ap)

import Data.Monoid
import Data.String (IsString(..))

import Prelude hiding (lookup)

-- An expression that may depend on values from an
-- environment
newtype RefFunc f s a = R { unR :: (s -> (f a)) -> f a }

instance IsString (RefFunc f String a) where
    fromString = lookup

instance (Applicative f, Num a) => Num (RefFunc f s a) where
    (+) = lift2 (+)
    (*) = lift2 (*)
    (-) = lift2 (-)
    abs = lift1 abs
    signum = lift1 signum
    negate = lift1 negate
    fromInteger = val . fromInteger

instance (Applicative f, Fractional a) => Fractional (RefFunc f s a) where
    (/) = lift2 (/)
    recip = lift1 recip
    fromRational = val . fromRational

instance (Applicative f, Monoid a) => Monoid (RefFunc f s a)  where
    mempty = val mempty
    mappend = lift2 mappend

-- | A constant value
val :: Applicative f => a -> RefFunc f s a
val = R . const . pure

-- | Create a new ref func
refFunc :: ((s -> (f a)) -> f a) -> RefFunc f s a
refFunc = R

-- | Get the function represented by a ref func
unRefFunc :: RefFunc f s a -> (s -> (f a)) -> f a
unRefFunc = unR

-- | Apply a function inside a ref func
ap :: Applicative f => f (a -> a) -> RefFunc f s a -> RefFunc f s a
ap f1 (R f2) = R $ \f -> f1 <*> (f2 f)

-- | An empty value, using empty
empty :: Alternative f => RefFunc f s a
empty = R $ const A.empty

-- | Lookup a value
lookup :: s -> RefFunc m s a
lookup s = R $ \f -> f s

-- | Multiply two RefFuncs 
multiply :: Applicative f => RefFunc f s a -> RefFunc f s b -> RefFunc f s (a, b)
multiply (R fa) (R fb) = R $ \f -> (,) <$> (fa $ fmap fst . f) <*> (fb $ fmap snd . f)

-- | Turn a RefFunc into a RefFunc on tuples by applying a pure function to 
--   obtain the second argument
split :: Applicative f => RefFunc f s a -> (a -> b) -> RefFunc f s (a, b)
split (R fa) f' = R $ \f -> let r = fa $ fmap fst . f in (,) <$> r <*> (fmap f' r) 

-- | Turn a RefFunc into a RefFunc on tuples by applying a monadic function to
--   obtain the second argument
split' :: (Functor f, Monad f) => RefFunc f s a -> (a -> RefFunc f s b) -> RefFunc f s (a, b)
split' (R fa) f' = R $ \f -> 
    let r = fa $ fmap fst . f in 
    let r' = fmap snd . f in 
    r >>= \a -> (unR $ f' a) r' >>= \b -> return (a, b)

-- | Auxiliary functions
lift2 :: Applicative f => (a -> a -> a) -> RefFunc f s a -> RefFunc f s a -> RefFunc f s a
lift2 op (R f1) (R f2) = R $ \m -> op <$> f1 m <*> f2 m

lift1 :: Applicative f => (a -> a) -> RefFunc f s a -> RefFunc f s a
lift1 op (R f1) = R $ \m -> op <$> f1 m