-- Types for expressions in MS Excel spreadsheets.
{-# LANGUAGE TupleSections #-}
module Spread.Excel.Types where

import Control.Applicative

import Spread.Core.RefFunc hiding (split')
import Spread.Core.Region
import Spread.Symbol.Grid

import Data.Foldable
import Data.Maybe (listToMaybe, catMaybes)
import Data.Monoid hiding (getFirst)

data ErrorType = Error
    deriving (Eq, Ord, Show)

data ExcelPrim = 
    TInt Integer
    | TDouble Double
    | TString String
    | TRat Rational
    | TError ErrorType
    deriving (Eq, Ord, Show)

data ExcelBinOp = 
    Plus
    | Minus
    | Times
    | Div
    deriving (Eq, Ord, Show)

binOp :: ExcelBinOp -> ExcelPrim -> ExcelPrim -> ExcelPrim
binOp o l r = case o of
    Plus -> maybe (TError Error) TInt $ (+) <$> (toInt l) <*> (toInt r)
    Minus -> maybe (TError Error) TInt $ (-) <$> (toInt l) <*> (toInt r)
    Times -> maybe (TError Error) TInt $ (*) <$> (toInt l) <*> (toInt r)
    Div -> maybe (TError Error) TRat $ (/) <$> (toRat l) <*> (toRat r)

data ExcelUnOp =
    Abs 
    deriving (Eq, Ord, Show)

toInt :: ExcelPrim -> Maybe Integer
toInt a = case a of
    TInt i -> Just i
    _      -> Nothing

toDouble :: ExcelPrim -> Maybe Double
toDouble a = case a of
    TDouble d -> Just d
    _         -> Nothing

toRat :: ExcelPrim -> Maybe Rational
toRat a = case a of
    TRat r -> Just r
    _      -> Nothing

data ExcelExpr =
    Prim ExcelPrim
    | Max Range
    | BinOp ExcelBinOp ExcelExpr ExcelExpr
    | UnOp ExcelUnOp ExcelExpr
    | VLookup ExcelExpr Range Int
    | CellRef Cell
    deriving (Eq, Ord)

interpret :: ExcelExpr -> RefFunc Maybe Cell ExcelPrim
interpret expr = case expr of
    Prim a -> val a
    Max  r -> refFunc $ \f -> 
        let err = return $ TError Error in
        let rws = rows r in
        let maxV' = getMax $ foldMap toMMax $ catMaybes $ fmap (\s -> s >>= f >>= toInt) $ fmap listToMaybe rws in
        maybe err return (maxV' >>= return . TInt)
    VLookup ex r i -> refFunc $ \f -> do
        let referenceValue = (unRefFunc $ interpret ex) f
        let rws = rows r
        let out (a, b) = b >>= return . (a,)
        (index,firstV) <- getFirst $ foldMap toMFirst $ filter ((==) referenceValue . snd) $ zipWith (,) [1..] $ fmap (\s -> s >>= f) $ fmap listToMaybe rws
        theRow <- safeElementAt rws index
        safeElementAt theRow (i-1) >>= f 
    CellRef c -> refFunc $ \f -> f c
    BinOp op l r -> lift2 (binOp op) (interpret l) (interpret r)


-- | Aggregations
newtype MMax a = MMax { getMax :: Maybe a }

toMMax :: a -> MMax a
toMMax = MMax . Just

instance (Ord a) => Monoid (MMax a) where
    mempty = MMax Nothing
    (MMax a) `mappend` (MMax b) = case (a, b) of
        (Just a, Nothing) -> MMax $ Just a
        (Nothing, Just a) -> MMax $ Just a 
        (Nothing, Nothing) -> MMax $ Nothing
        (Just a, Just b) -> MMax $ Just $ max a b

newtype MFirst a = MFirst { getFirst :: Maybe a }

toMFirst :: a -> MFirst a
toMFirst = MFirst . Just

instance Monoid (MFirst a) where
    mempty = MFirst Nothing
    l `mappend` r = maybe r (const $ l) $ getFirst l

safeElementAt :: [a] -> Int -> Maybe a
safeElementAt l i = case l of
    []     -> Nothing
    (x:xs) -> if (i <= 0) then (Just x) else (safeElementAt xs (i - 1))