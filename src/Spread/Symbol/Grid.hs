module Spread.Symbol.Grid
    (Row,
     firstRow,
     lastRow,
     makeRow,
     Column,
     firstColumn,
     lastColumn,
     makeColumn,
     Cell,
     row,
     column,
     origin,
     up,
     down,
     left,
     right,
     topRight,
     bottomRight,
     bottomLeft,
     showExcel,
     Range,
     range,
     rows,
     normalise,
     inRange
     )
 where

import qualified Data.Char as C
import qualified Data.Map.Lazy as M

newtype Row = ARow { _rowNumber :: Int }
    deriving (Eq, Ord)

instance Enum Row where
    toEnum = ARow
    fromEnum = _rowNumber

instance Show Row where
    show = show . _rowNumber

makeRow :: Int -> Row
makeRow i = ARow $ max (min maxRow i) minRow

firstRow :: Row
firstRow = makeRow minRow

lastRow :: Row
lastRow = makeRow maxRow

previousRow :: Row -> Row
previousRow (ARow i) = makeRow $ i - 1

nextRow :: Row -> Row
nextRow (ARow i) = makeRow $ i + 1

newtype Column = AColumn { _columnIndex :: Int  }
    deriving (Eq, Ord)

instance Enum Column where
    toEnum = AColumn
    fromEnum = _columnIndex

instance Show Column where
    show =  intToCol . _columnIndex

makeColumn :: String -> Column
makeColumn = makeColumn' . colToInt

makeColumn' :: Int -> Column
makeColumn' i = AColumn $ max (min maxCol i) minCol

firstColumn :: Column
firstColumn = makeColumn' minCol

lastColumn :: Column
lastColumn = makeColumn' maxCol

previousColumn :: Column -> Column
previousColumn (AColumn i) = makeColumn' $ i  - 1

nextColumn :: Column -> Column
nextColumn (AColumn i) = makeColumn' $ i + 1


-- | Operations on cells

newtype Cell = Cell { unCell :: (Row, Column) }
    deriving (Eq, Ord)

row :: Cell -> Row
row = fst . unCell

column :: Cell -> Column
column = snd . unCell

cell :: Row -> Column -> Cell
cell = curry Cell

-- The first cell
origin :: Cell
origin = Cell (firstRow, firstColumn)

left :: Cell -> Cell
left (Cell (r, c)) = Cell (r, previousColumn c)

right :: Cell -> Cell
right (Cell (r, c)) = Cell (r, nextColumn c)

up :: Cell -> Cell
up (Cell (r, c)) = Cell (previousRow r, c)

down :: Cell -> Cell
down (Cell (r, c)) = Cell (nextRow r, c)

topRight :: Cell
topRight = Cell (firstRow, lastColumn)

bottomRight :: Cell
bottomRight = Cell (lastRow, lastColumn)

bottomLeft :: Cell
bottomLeft = Cell (lastRow, firstColumn)

-- | Show the coordinates of a cell in Excel style
showExcel :: Cell -> String
showExcel (Cell (r, c)) = show c ++ show r

-- Internals
minRow :: Int
minRow = 1

minCol :: Int
minCol = colToInt "A"

-- maxRow as of Excel version 15.0.4631.1002
maxRow :: Int
maxRow = 1048576

-- maxCol as of Excel version 15.0.4631.1002
maxCol :: Int
maxCol = colToInt "XFD"

allRows :: [Row]
allRows = fmap makeRow [minRow .. maxRow]

allColumns :: [(Int,String)]
allColumns = zipWith (,) [1..] columnNames where
    columnNames = dropWhile (not . (==) "A") $ takeUntil (== "XFD") columns'
    capitals = fmap return ['A' .. 'Z']
    columns' = capitals ++ (concat $ fmap (\pr -> fmap (pr ++) capitals) columns')

takeUntil :: (a -> Bool) -> [a] -> [a]
takeUntil pr l = case l of
    [] -> []
    x:xs -> if (pr x) then [x] else x:(takeUntil pr xs)

colToInt :: String -> Int
colToInt = foldl f 0 where
    f n d = 26 * n + (C.ord d - 64)

intToCol :: Int -> String
intToCol i = 
    let (d, m) = divMod (i-1) 26 in
    let ch = C.chr (m + 65) in
    if (d > 0) then
        intToCol d ++ [ch]
    else [ch]

col' :: [(Int, String, Int, String)]
col' = fmap (\(i, s) -> (i, s, colToInt s, intToCol i)) allColumns

-- If I implemented it correctly then this list should be empty.
differences :: [(Int, String, Int, String)]
differences = filter (\(i, s, i', s') -> (not (i == i') || not (s == s'))) col'

-- A range is a rectangle defined by two cells.
newtype Range = Range { getRange :: (Cell, Cell) }
    deriving (Eq, Ord)

-- Create range
range :: Cell -> Cell -> Range
range = curry Range

-- Normalise a range so that the first coordinate is top-left
-- and the second coordinate is bottom-right
normalise :: Range -> Range
normalise (Range (c1,c2)) = range c1' c2' where
    c1' = cell (min (row c1) (row c2)) (min (column c1) (column c2))
    c2' = cell (max (row c1) (row c2)) (max (column c1) (column c2))

-- Enumerate the cells in a range by row
rows :: Range -> [[Cell]]
rows (Range (c1, c2)) = [ [(cell r c) |  c <- [firstCol .. lastCol]] | r <- [firstRow .. lastRow]] where
    firstRow = min (row c1) (row c2)
    lastRow = max (row c1) (row c2)
    firstCol = min (column c1) (column c2)
    lastCol = max (column c1) (column c2)

inRange :: Range -> Cell -> Bool
inRange r c = (column c1 <= co) && (column c2 >= co) && (row c1 <= ro) && (row c2 >= ro) where
    Range (c1, c2) = normalise r
    ro = row c
    co = column c