module Spread.Expression.Pretty where

import Spread.Expression.Language

-- Wrapper around interpreted results of expression language
newtype Pretty a = P { unP :: String }
    deriving (Eq, Ord)

instance Show (Pretty a) where
    show = unP

instance Symantics Pretty where
    lam f = P $ "\\x -> " ++ (unP $ f $ P "x")
    app (P f) (P v) = P $ "(" ++ f ++ ") (" ++ v ++ ")"

instance PrimsY Pretty where
    int = P . show
    str = P

instance NumY Pretty where
    add (P l) (P r) = P $ l ++ " + " ++ r
    mult (P l) (P r) = P $ "(" ++ l ++ ") * (" ++ r ++ ")"
    sub (P l) (P r) = P $ "(" ++ l ++ ") - (" ++ r ++ ")"
    neg (P l) = P $ "-" ++ l
    abss (P l) = P $ "abs(" ++ l ++ ")"
    sign (P l) = P $ "signum(" ++ l ++ ")"