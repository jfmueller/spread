module Spread.Expression.Endofunction where

import Control.Applicative
import Control.Monad

data Endo b = 
    J b 
    | F (b -> Endo b)

instance (Show b) => Show (Endo b) where
    show (J b) = "J " ++ show b
    show (F _) = "F"

f1 :: Endo Integer
f1 = F $ \s -> J (s + 10)

unwrapVal :: (Alternative f) => Endo b -> f b
unwrapVal f = case f of
    (J a) -> pure a
    (F _) -> empty

unwrapEndo :: (Alternative f) => Endo b -> f (b -> Endo b)
unwrapEndo f = case f of
    (F f) -> pure $ \b -> f b
    _ -> empty
    
apply :: (Alternative f) => Endo b -> Endo b -> f (Endo b)
apply l r = (unwrapEndo l) <*> (unwrapVal r)
