module Spread.Expression.Language where

-- * Core language
class Symantics repr where
    lam  :: (repr a -> repr a) -> repr (a->a)
    app  :: repr (a->a) -> repr a -> repr a

class NumY repr where
    add  :: Num a => repr a -> repr a -> repr a
    mult :: Num a => repr a -> repr a -> repr a
    sub  :: Num a => repr a -> repr a -> repr a
    neg  :: Num a => repr a -> repr a
    abss :: Num a => repr a -> repr a
    sign :: Num a => repr a -> repr a

class PrimsY repr where
    int :: Integer -> repr Integer
    str :: String  -> repr String

t1 :: (Symantics repr, NumY repr, PrimsY repr) => repr Integer
t1 = (int 100) `add` (int 100)    

t2 :: (Symantics repr, NumY repr, PrimsY repr) => repr (Integer -> Integer)
t2 = lam $ \i -> (int 20) `add` i

