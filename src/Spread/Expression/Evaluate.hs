module Spread.Expression.Evaluate where

import Spread.Expression.Language

-- Wrapper around interpreted results of expression language
newtype EvaluateAs a = E { unE :: a }
    deriving (Eq, Ord)

instance (Show a) => Show (EvaluateAs a) where
    show = show . unE

instance Symantics EvaluateAs where
    lam f = E (unE . f . E)
    app (E f) = E . f . unE

instance PrimsY EvaluateAs where
    str = E
    int = E

instance NumY EvaluateAs where
    add (E l) (E r) = E (l + r)
    mult (E l) (E r) = E (l * r)
    sub (E l) (E r) = E (l - r)
    neg (E n) = E $ negate n
    abss (E n) = E $ abs n
    sign (E n) = E $ signum n